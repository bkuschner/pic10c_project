# README #

Project: Hockey Game Simulator

My goal is to create a program that will simulate hockey games using National Hockey League (NHL) teams and players. While I do not expect my simulator to be able to accurately predict the outcomes of future
hockey games, I would like it to determine the outcome logically based on the skills of each team and possibly some luck. The focus will be on demonstrating my programming skills that I have learned from the PIC 10 series, and not on the methodology for determining the outcomes of hockey games.

I will be using topics discussed in class such as version control, proper memory management, standard containers, and generic algorithms.