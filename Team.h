#ifndef TEAM_H
#define TEAM_H

#include<vector>
#include<string>
#include "Player.h"

class Team {
private:
	std::string name;
	std::vector<Player> players;
	int o_rating;
	int d_rating;
	int discipline;
	int wins;
	int losses;

	void calcRatings();
public:
	Team(std::string i_name, std::vector<Player> i_players);
	bool removePlayer(std::string name);
	void addPlayer(Player newPlayer);
	void addWin();
	void addLoss();
	std::string getName();
	std::vector<int> getRatings(); //{o_rating, d_rating, discipline}
	std::vector<int> getRecord(); //{wins, losses}
};

#endif