#include "Player.h"
#include "Team.h"
#include <iostream>
#include <algorithm>

int main() {
	std::vector<Team> teams = std::vector<Team>();

	//2 premade teams to play with. included top 2 lines of players.
	std::vector<Player> ducks_players = std::vector<Player>();
	ducks_players.push_back(Player("Ryan Getzlaf", 15, 1, 100, 100, 100));
	ducks_players.push_back(Player("Corey Perry", 10, 1, 100, 75, 70));
	ducks_players.push_back(Player("Rickard Rakell", 67, 1, 94, 87, 97));
	ducks_players.push_back(Player("Ryan Kesler", 17, 1, 90, 100, 80));
	ducks_players.push_back(Player("Andrew Cogliano", 7, 1, 80, 95, 100));
	ducks_players.push_back(Player("Jakob Silfverberg", 33, 1, 90, 93, 95));
	ducks_players.push_back(Player("Cam Fowler", 4, 2, 98, 90, 96));
	ducks_players.push_back(Player("Brandon Montour", 26, 2, 90, 93, 95));
	ducks_players.push_back(Player("Francois Beauchemin", 23, 2, 80, 98, 90));
	ducks_players.push_back(Player("Hampus Lindholm", 47, 2, 92, 92, 98));
	ducks_players.push_back(Player("John Gibson", 36, 3, 90, 95, 100));
	teams.push_back(Team("Anaheim Ducks", ducks_players));

	std::vector<Player> kings_players = std::vector<Player>();
	kings_players.push_back(Player("Anze Kopitar", 11, 1, 100, 100, 100));
	kings_players.push_back(Player("Alex Iafallo", 19, 1, 90, 90, 90));
	kings_players.push_back(Player("Dustin Brown", 23, 1, 98, 88, 91));
	kings_players.push_back(Player("Jeff Carter", 77, 1, 90, 100, 100));
	kings_players.push_back(Player("Tanner Pearson", 70, 1, 84, 91, 90));
	kings_players.push_back(Player("Adrian Kempe", 9, 1, 86, 92, 95));
	kings_players.push_back(Player("Derek Forbort", 24, 2, 85, 96, 97));
	kings_players.push_back(Player("Drew Doughty", 8, 2, 100, 92, 99));
	kings_players.push_back(Player("Dion Phaneuf", 3, 2, 98, 81, 91));
	kings_players.push_back(Player("Alec Martinez", 27, 2, 84, 93, 78));
	kings_players.push_back(Player("Jonathan Quick", 32, 3, 95, 99, 100));
	teams.push_back(Team("Los Angeles Kings", kings_players));

	std::cout << "Welcome to NHL simulator! I've already made 2 teams for you:"
		<< std::endl << teams.at(0).getName() << std::endl << teams.at(1).getName() << std::endl;

	std::string command = "";	
	std::cout << "Type add to add a team, play to play a game, end to end" << std::endl;
	std::cin >> command;
	while(command != "end" && command != "add" && command != "play") {
		std::cout << "Invalid command. add to add a team, play to play a game, end to end"
			<< std::endl;
		std::cin >> command;
	}
	while(command != "end") {
		if(command == "play") {
			std::string h = "";
			std::string a = "";
			std::cin.ignore();
			std::cout << "Home Team?" << std::endl;
			std::getline(std::cin, h);
			auto homeMatch = [&h](Team t) {
				if(t.getName() == h) return true;
				else return false;
			};
			auto homeIter = std::find_if(teams.begin(), teams.end(), homeMatch);			
			while(homeIter == teams.end()) {
				std::cout << "team not found. try again" << std::endl;
				std::getline(std::cin, h);
				homeIter = std::find_if(teams.begin(), teams.end(), homeMatch);
			}
			Team home = *homeIter;

			std::cout << "Away Team?" << std::endl;
			std::getline(std::cin, a);
			std::cout << a << std::endl;
			auto awayMatch = [&a](Team t) {
				if(t.getName() == a) return true;
				else return false;
			};
			auto awayIter = std::find_if(teams.begin(), teams.end(), awayMatch);
			while(awayIter == teams.end()) {
				std::cout << "team not found. try again" << std::endl;
				std::getline(std::cin, a);
				awayIter = std::find_if(teams.begin(), teams.end(), awayMatch);
			}
			Team away = *awayIter;
			//note: intentionally able to have a team play against itself

			int advantage = 0;
			std::vector<int> hratings = home.getRatings();
			std::vector<int> aratings = away.getRatings();
			if(hratings.at(0) >= aratings.at(0)) advantage++; //notice home team wins in tie rating
			if(hratings.at(1) >= aratings.at(1)) advantage++;
			if(hratings.at(2) >= aratings.at(2)) advantage++;
			if(advantage >= 2) {
				std::cout << home.getName() + " win!" << std::endl;
				home.addWin();
				away.addLoss();
			}
			else {
				std::cout << away.getName() + " win!" << std::endl;
				home.addLoss();
				away.addWin();
			}
			std::cout << "The records of the two teams now are:" << std::endl
					<< home.getName() << ": " << home.getRecord().at(0) << "-" << home.getRecord().at(1)
					<< std::endl << away.getName() << ": " << away.getRecord().at(0)
					+ "-" + away.getRecord().at(1) << std::endl;

		} //end of if play

		if(command == "add") {
			std::cin.ignore();
			std::vector<Player> thePlayers = std::vector<Player>();
			std::string teamName = "";
			std::string name = "";
			int number, pos, orating, drating, disc;
			std::cout << "Name of the team?" << std::endl;
			std::getline(std::cin, teamName);
			std::cout << "Player name? type done if done" << std::endl;
			std::getline(std::cin, name);
			while(name != "done") {			
				std::cout <<"number?" << std::endl;
				std::cin >> number;
				std::cout << "position? 1 for forward, 2 for defenseman, 3 for goalkeeper" << std::endl;
				std::cin >> pos;
				std::cout << "offensive rating? in range [1,100]" << std::endl;
				std::cin >> orating;
				std::cout << "defensive rating? in range [1,100]" << std::endl;
				std::cin >> drating;
				std::cout << "discipline rating? in range [1,100]" << std::endl;
				std::cin >> disc;
				thePlayers.push_back(Player(name, number, pos, orating, drating, disc));
				std::cout << "Player name? type done if done" << std::endl;
				std::cin.ignore();
				std::getline(std::cin, name);
			}
			teams.push_back(Team(teamName, thePlayers));
		} // end of if add

		std::cout << "Type add to add a team, play to play a game, end to end" << std::endl;
		std::cin >> command;
		while(command != "end" && command != "add" && command != "play") {
			std::cout << "Invalid command. add to add a team, play to play a game, end to end"
				<< std::endl;
			std::cin >> command;
		}
	}
}