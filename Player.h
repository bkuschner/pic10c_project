#ifndef PLAYER_H
#define PLAYER_H

#include<string>

class Player {
private:
	std::string name;
	int number;
	int position; //1 is forward, 2 is defenseman, 3 is goalkeeper
	int o_rating; //ratings should be in the range [1,100]
	int d_rating;
	int discipline;

public:
	Player(std::string in_name, int in_number, int in_pos, int o, int d
		, int disc);
	std::string getName();
	int getNumber();
	int getPosition();
	int getORating();
	int getDRating();
	int getDiscipline();
};

#endif