#include "Team.h"
#include<iterator>

/* constructor for team
@param i_players the inital players on the team
*/
Team::Team(std::string i_name, std::vector<Player> i_players) {
	name = i_name;
	players = i_players;
	wins = 0;
	losses = 0;
	calcRatings();
}

/* calculate the team's ratings and adjust accordingly
*/
void Team::calcRatings() {
	int offensive = 0;
	int defensive = 0;
	int discipline = 0;
	int numPlayers = players.size();
	auto first = players.begin();
	auto last = players.end();
	while(first != last){ //accumulate the sums of the player ratings
		offensive += (*first).getORating();
		defensive += (*first).getDRating();
		discipline += (*first).getDiscipline();
		++first;
	}

	o_rating = (offensive / numPlayers);
	d_rating = (defensive / numPlayers);
	discipline = (discipline / numPlayers);
}

/* remove a player. since I store players in a vector, this operation will
* be O(n) time complexity. If I had used a binary search tree it would be
* O(log n ). If I had used a hash map, it would be O(1). The high time
* complexity should not be a problem because the size of players should be
* about 20 or less
@param name name of player to be removed
@return true if the removal was successful
*/
bool Team::removePlayer(std::string name) {
	auto first = players.begin();
	auto last = players.end();
	while(first != last) {
		if((*first).getName() == name) {
			first = players.erase(first);
			calcRatings();
			return true;
		}
		++first;
	}
	return false;
}

/* add a player to the team
@param newPlayer player to be added to the team
*/
void Team::addPlayer(Player newPlayer) {
	players.push_back(newPlayer);
	calcRatings();
}

/*add a win to the team's record
*/
void Team::addWin() {
	++wins;
}

/*add a loss to the team's record
*/
void Team::addLoss() {
	++losses;
}

/* gets the name of the team
@return the name of the team
*/
std::string Team::getName() {
	return name;
}

/* gets the team's ratings
@return vector with ratings in format {o_rating, d_rating, discipline}
*/
std::vector<int> Team::getRatings() {
	std::vector<int> ratings = std::vector<int>();
	ratings.push_back(o_rating);
	ratings.push_back(d_rating);
	ratings.push_back(discipline);
	return ratings;
}

/* gets the team's record
@return vector with record in format {wins, losses}
*/
std::vector<int> Team::getRecord() {
	std::vector<int> record = std::vector<int>();
	record.push_back(wins);
	record.push_back(losses);
	return record;
}