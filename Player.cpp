#include "Player.h"

/* constructor for Player
@param in_name the player's name
@param in_number the player's number (the one on the back of his jersey)
@param in_pos the player's positon 1:forward, 2:defenseman, 3:goalkeeper
@param o the player's offensive rating in [1,100]
@param d the player's defensive rating in [1,100]
@param disc the player's discipline rating in [1, 100]
*/
Player::Player(std::string in_name, int in_number, int in_pos, int o, int d
	, int disc) :
	name(in_name), number(in_number), position(in_pos), o_rating(o)
	, d_rating(d), discipline(disc) {}

/* gets the player's name
@return the player's name
*/
std::string Player::getName() { return name;}

/* gets the player's number
@return the player's number
*/
int Player::getNumber() {return number;}

/* gets the player's position
@return the player's position
*/
int Player::getPosition() {return position;}

/* gets the player's offensive rating
@return the player's offensive rating
*/
int Player::getORating() {return o_rating;}

/* gets the player's defensive rating
@return the player's defensive rating
*/
int Player::getDRating() {return d_rating;}

/* gets the player's discipline rating
@return the player's discipline rating
*/
int Player::getDiscipline() {return discipline;}